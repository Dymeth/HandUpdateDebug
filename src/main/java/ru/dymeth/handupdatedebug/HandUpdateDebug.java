package ru.dymeth.handupdatedebug;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import ru.dymeth.handupdatedebug.packet.WrapperPlayServerSetSlot;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public final class HandUpdateDebug extends JavaPlugin {
    private final String prefix = "[" + this.getName() + "] ";
    private Class<?> pluginClassLoaderClass;
    private Method getPluginMethod;

    @Override
    public void onEnable() {
        try {
            this.pluginClassLoaderClass = this.getClass().getClassLoader().getClass();
            this.getPluginMethod = this.pluginClassLoaderClass.getDeclaredMethod("getPlugin");
        } catch (Throwable t) {
            throw new RuntimeException(this.prefix + "Неподдерживаемая версия сервера", t);
        }
        ProtocolLibrary.getProtocolManager().addPacketListener(
                new PacketAdapter(this, ListenerPriority.NORMAL, PacketType.Play.Server.SET_SLOT) {
                    @Override
                    public void onPacketSending(PacketEvent event) {
                        HandUpdateDebug.this.onPacketSending(event);
                    }
                });

    }

    public void onPacketSending(PacketEvent event) {
        Player player = event.getPlayer();
        if (!player.isOp()) return;
        if (player.getOpenInventory().getTopInventory().getType() != InventoryType.CRAFTING) return;
        WrapperPlayServerSetSlot packet = new WrapperPlayServerSetSlot(event.getPacket());
        int changedSlot = packet.getSlot() - 9 * 4;
        int handSlot = player.getInventory().getHeldItemSlot();
        if (changedSlot != handSlot) return;
        try {
            Set<String> possiblePlugins = new HashSet<>();
            RuntimeException exception = new RuntimeException(this.prefix + "Отладка (это не ошибка!)");
            for (StackTraceElement element : exception.getStackTrace()) {
                ClassLoader loader = Class.forName(element.getClassName()).getClassLoader();
                if (!(this.pluginClassLoaderClass.isInstance(loader))) continue;
                possiblePlugins.add(((Plugin) this.getPluginMethod.invoke(loader)).getName());
            }
            possiblePlugins.remove(this.getName());
            possiblePlugins.remove("ProtocolLib");
            if (possiblePlugins.size() == 0) {
                player.sendMessage(this.prefix + "Не удалось определить плагин, изменивший предмет (его изменило ядро?). Для включения режима отладки зажмите шифт. Отладка будет отправлена в консоль");
            } else if (possiblePlugins.size() == 1) {
                player.sendMessage(this.prefix + "Предмет в руке обновил плагин " + possiblePlugins.iterator().next());
            } else {
                player.sendMessage(this.prefix + "Предмет в руке обновил один из этих плагинов: "
                        + String.join(", ", possiblePlugins));
            }
            if (player.isSneaking()) exception.printStackTrace();
        } catch (Throwable t) {
            player.sendMessage(this.prefix + "Не удалось определить причину обновления предмета в руке из-за ошибки, см. консоль");
            this.getLogger().warning("Не удалось определить причину обновления предмета в руке из-за ошибки:");
            t.printStackTrace();
        }
    }
}
